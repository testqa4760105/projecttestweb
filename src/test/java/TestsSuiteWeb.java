import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

public class TestsSuiteWeb extends AbstractTest {

    @Test
    void authPositive(){
        new Auth(getWebDriver())
                .loginIn("Igor77777","8ea688dec0");
        Assertions.assertDoesNotThrow( ()-> getWebDriver().navigate().to("https://test-stand.gb.ru"),
                "Ошибка авторизации");
        Assertions.assertTrue(getWebDriver().findElement(By.xpath(".//div/main/nav/a/span")).getText().equals("Home"));
        Assertions.assertTrue(getWebDriver().findElement(By.xpath(".//button[@aria-checked='false']")).getAttribute("aria-checked").equals("false"));
    }

    @Test
    void authNegativeShortLogin(){
        new Auth(getWebDriver())
                .loginIn("ar","c582dec943");
        Assertions.assertDoesNotThrow( ()-> getWebDriver().navigate().to("https://test-stand.gb.ru"),
                "Ошибка авторизации");
        Assertions.assertFalse(getWebDriver().findElement(By.xpath(".//div/main/nav/a/span")).getText().equals("Home"));
        Assertions.assertFalse(getWebDriver().findElement(By.xpath(".//button[@aria-checked='false']")).getAttribute("aria-checked").equals("false"));
    }

    @Test
    void authNegativeBordersShortLogin(){
        new Auth(getWebDriver())
                .loginIn("are","4015e9ce43");
        Assertions.assertDoesNotThrow( ()-> getWebDriver().navigate().to("https://test-stand.gb.ru"),
                "Ошибка авторизации");
        Assertions.assertTrue(getWebDriver().findElement(By.xpath(".//div/main/nav/a/span")).getText().equals("Home"));
        Assertions.assertTrue(getWebDriver().findElement(By.xpath(".//button[@aria-checked='false']")).getAttribute("aria-checked").equals("false"));
    }

    @Test
    void authNegativeLongLogin(){
        new Auth(getWebDriver())
                .loginIn("testtesttesttesttesttest","8dbf707694");
        Assertions.assertDoesNotThrow( ()-> getWebDriver().navigate().to("https://test-stand.gb.ru"),
                "Ошибка авторизации");
        Assertions.assertFalse(getWebDriver().findElement(By.xpath(".//div/main/nav/a/span")).getText().equals("Home"));
        Assertions.assertFalse(getWebDriver().findElement(By.xpath(".//button[@aria-checked='false']")).getAttribute("aria-checked").equals("false"));
    }

    @Test
    void authNegativeBordersLongLogin(){
        new Auth(getWebDriver())
                .loginIn("testesttesttesttest","1918580e51");
        Assertions.assertDoesNotThrow( ()-> getWebDriver().navigate().to("https://test-stand.gb.ru"),
                "Ошибка авторизации");
        Assertions.assertTrue(getWebDriver().findElement(By.xpath(".//div/main/nav/a/span")).getText().equals("Home"));
        Assertions.assertTrue(getWebDriver().findElement(By.xpath(".//button[@aria-checked='false']")).getAttribute("aria-checked").equals("false"));
    }

    @Test
    void nextPage(){
        new Auth(getWebDriver())
                .loginIn("Igor77777","8ea688dec0");
        new NextPrevPage(getWebDriver())
                .creationPost(true);
        Assertions.assertDoesNotThrow( ()-> getWebDriver().navigate().to("https://test-stand.gb.ru"),
                "Ошибка авторизации");
        Assertions.assertTrue(getWebDriver().findElement(By.xpath(".//div/main/nav/a/span")).getText().equals("Home"));
        Assertions.assertTrue(getWebDriver().findElement(By.xpath(".//button[@aria-checked='false']")).getAttribute("aria-checked").equals("false"));
    }

    @Test
    void prevPage(){
        new Auth(getWebDriver())
                .loginIn("Igor77777","8ea688dec0");
        new NextPrevPage(getWebDriver())
                .creationPost(false);
        Assertions.assertDoesNotThrow( ()-> getWebDriver().navigate().to("https://test-stand.gb.ru"),
                "Ошибка авторизации");
        Assertions.assertTrue(getWebDriver().findElement(By.xpath(".//div/main/nav/a/span")).getText().equals("Home"));
        Assertions.assertTrue(getWebDriver().findElement(By.xpath(".//button[@aria-checked='false']")).getAttribute("aria-checked").equals("false"));
    }

    @Test
    void verificationPost(){
        new Auth(getWebDriver())
                .loginIn("Igor77777","8ea688dec0");
        Assertions.assertDoesNotThrow( ()-> getWebDriver().navigate().to("https://test-stand.gb.ru"),
                "Ошибка авторизации");
        Assertions.assertTrue(getWebDriver().findElement(By.xpath(".//a[@href='/posts/13384']/h2")).getText().equals("test13"));
        Assertions.assertTrue(getWebDriver().findElement(By.xpath(".//a[@href='/posts/13384']/div")).getText().equals("test13"));
    }
}
