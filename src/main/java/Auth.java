import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Auth extends AbstractPage{
    @FindBy(xpath = ".//button/span")
    private WebElement buttonLogin;
    @FindBy(xpath = ".//input[@type='text']")
    private WebElement fieldUsername;
    @FindBy(xpath = ".//input[@type='password']")
    private WebElement fieldPassword;


    public Auth(WebDriver driver) {
        super(driver);
    }
    public void loginIn(String login, String password){

        Actions loginIn = new Actions(getDriver());
        loginIn
                .click(this.fieldUsername)
                .sendKeys(this.fieldUsername,login)
                .click(this.fieldPassword)
                .sendKeys(this.fieldPassword, password)
                .pause(2000l)
                .moveToElement(this.buttonLogin)
                .click(this.buttonLogin)
                .moveToElement(this.buttonLogin)
                .click(this.buttonLogin)
                .moveToElement(this.buttonLogin)
                .click(this.buttonLogin)
                .moveToElement(this.buttonLogin)
                .click(this.buttonLogin)
                .moveToElement(this.buttonLogin)
                .click(this.buttonLogin)
                .build()
                .perform();

    }
}
