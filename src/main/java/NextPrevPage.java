import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class NextPrevPage extends AbstractPage{
    @FindBy(xpath = ".//a[@href='/?page=2']")
    private WebElement nextPage;
    @FindBy(xpath = ".//a[@class='svelte-d01pfs disabled']")
    private WebElement prevPage;

    public NextPrevPage(WebDriver driver) {
        super(driver);
    }

    public void creationPost(boolean nextPrevPage){
        Actions creationPost = new Actions(getDriver());
        if(nextPrevPage) {
        creationPost
                .pause(2000l)
                .moveToElement(this.nextPage)
                .pause(3000l)
                .click(this.nextPage)
                .build()
                .perform();
        } else {
            creationPost
                    .pause(2000l)
                    .moveToElement(this.prevPage)
                    .pause(3000l)
                    .click(this.prevPage)
                    .build()
                    .perform();
        }


    }

}
